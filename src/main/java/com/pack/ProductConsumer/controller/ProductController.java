package com.pack.ProductConsumer.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pack.ProductConsumer.entity.Product;
import com.pack.ProductConsumer.repository.ProductRepository;

@RestController
public class ProductController {
	@Autowired
	ProductRepository repo;
	
	@RequestMapping(value="/products",method=RequestMethod.GET)
	public List<Product> getAllProducts() {
		return (List<Product>) repo.findAll();
	}
	@GetMapping("/products/{id}")
	public Product  findProductByTd(@PathVariable("id") Integer pid) {
		Optional<Product> opt = repo.findById(pid);
		return opt.get();
	}
	@PostMapping("/products")
	public Product saveProduct(@RequestBody Product product) {
		return repo.save(product);
	}

}



