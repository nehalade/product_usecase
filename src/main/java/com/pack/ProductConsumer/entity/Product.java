package com.pack.ProductConsumer.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Id;

@Entity
@Table(name="product")
public class Product {
	@Id
	private Integer Id;
	private String name;
	private Integer quantity;
	private String brand;
	private Double price;
	@DateTimeFormat(pattern="YYYY/MM/DD")
	private Date   expdate;
	@DateTimeFormat(pattern="YYYY/MM/DD")
	private Date  mgfdate;	
	public Product() {
		super();
	}
	public Product(Integer id, String name, Integer quantity, String brand, Double price, Date expdate, Date mgfdate) {
		super();
		Id = id;
		this.name = name;
		this.quantity = quantity;
		this.brand = brand;
		this.price = price;
		this.expdate = expdate;
		this.mgfdate = mgfdate;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Date getExpdate() {
		return expdate;
	}
	public void setExpdate(Date expdate) {
		this.expdate = expdate;
	}
	public Date getMgfdate() {
		return mgfdate;
	}
	public void setMgfdate(Date mgfdate) {
		this.mgfdate = mgfdate;
	}
	
	
	
}
