package com.pack.ProductConsumer.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pack.ProductConsumer.entity.Product;

@Repository
public interface ProductRepository  extends CrudRepository<Product, Integer>{

	
	
}
